// DO NOT REMOVE THIS PART ------------------------------------------

// UNO
// Version: 1.0
// Build: 221
// Stage: FINAL
// Sole programmer: YEO YIH TANG
// Year: 2010
// Date of release 30 April 2010
// Released at: http://yihtang0711-prog.blogspot.com
// Contact (email): yeoyihtang@live.com

//--------------------------------------------------------------------

#include <iostream>
#include <iomanip>
#include <cstring>
#include <ctime>
using namespace std;
int distrindex = 0; // global variable

struct unocard
{
    char name[20];
    char colour[10];
};
struct cardinhand
{
    char name[20];
    char colour[10];
};
struct player
{
	int num_card; // numbers of card in hand
	cardinhand playcard[108]; // maximum of 108 card in hand
};

// function list
void displaycard(player, int);
void putcard(player, int &, int, char [], char [], bool&);
void drawcard(unocard [], player [], int, char[], char[], bool&);
void swapcard(int, player [], int, int);
void drawTwo (unocard[], player[], int);
void drawFour (unocard[], player[], int, bool, unocard, int &);
void chooseColour (char[], char[], int, player);
void randomsort (unocard[]);
void coloursort (player[]);
int findpower (int);

int main ()
{
    unocard card[108], cardontable, previouscard;
	player p[4];
	system("title UNO Game version 1.0 [Programmed by Yeo Yih Tang]");
	system("color F0");

	int indexstored[108]; // for NOT to repeat the same ID again.
	//bool repeatrandgen;
	bool win, lose;
	int card_choice;
	bool putfail, playsuccess, dontproceed, blackplay;
	int playernum, drawTwoVictim;
	char userans[100];

	char replych2[100];
    bool repeatchoose2;
    int numpower2, powerresult2;

    /////////////////////////////// assigning values to all cards ///////////////////////////////////
    // assign numbers
    for (int assg = 0; assg < 4; assg++)
        strcpy(card[assg].name,"0");
    for (int assg = 4; assg < 12; assg++)
        strcpy(card[assg].name, "1");
    for (int assg = 12; assg < 20; assg++)
        strcpy(card[assg].name, "2");
    for (int assg = 20; assg < 28; assg++)
        strcpy(card[assg].name, "3");
    for (int assg = 28; assg < 36; assg++)
        strcpy(card[assg].name, "4");
    for (int assg = 36; assg < 44; assg++)
        strcpy(card[assg].name, "5");
    for (int assg = 44; assg < 52; assg++)
        strcpy(card[assg].name, "6");
    for (int assg = 52; assg < 60; assg++)
        strcpy(card[assg].name, "7");
    for (int assg = 60; assg < 68; assg++)
        strcpy(card[assg].name, "8");
    for (int assg = 68; assg < 76; assg++)
        strcpy(card[assg].name, "9");
    for (int assg = 76; assg < 84; assg++)
        strcpy(card[assg].name, "Reverse");
    for (int assg = 84; assg < 92; assg++)
        strcpy(card[assg].name, "Draw Two");
    for (int assg = 92; assg < 100; assg++)
        strcpy(card[assg].name, "Skip");
    for (int assg = 100; assg < 104; assg++)
        strcpy(card[assg].name, "Draw Four");
    for (int assg = 104; assg < 108; assg++)
        strcpy(card[assg].name, "Wild");
    // assign status

    // assign colour
    for (int assgc = 0; assgc < 100; assgc++)
    {
        if (assgc % 4 == 0)
            strcpy(card[assgc].colour, "Red");
        else if (assgc % 4 == 1)
            strcpy(card[assgc].colour, "Yellow");
        else if (assgc % 4 == 2)
            strcpy(card[assgc].colour, "Blue");
        else if (assgc % 4 == 3)
            strcpy(card[assgc].colour, "Green");
    }
    for (int assgc = 100; assgc < 108; assgc++)
        strcpy (card[assgc].colour, "Black");

    ///////////////////////////////////// end of assigning numbers//////////////////////////////////////////
    bool exitprog, backtomenu;
    bool repeatgame;

    char replych[100];
    bool repeatchoose;
    int reply;
    int numpower, powerresult;

    do
    {
        system("color F0");

        cout << endl;
        cout << "       oo    oo     ooo    oo      ooooo   TM" << endl;
        cout << "       oo    oo     oooo   oo    oo     oo  " << endl;
        cout << "       oo    oo     oo oo  oo    oo     oo  " << endl;
        cout << "       oo    oo     oo  oo oo    oo     oo   " << endl;
        cout << "       oo    oo     oo   oooo    oo     oo        VERSION 1.0" << endl;
        cout << "        oooooo      oo    ooo      ooooo          BUILD 221  " << endl << endl;
        cout << " Disclaimer: This product name is not belonged to Yeo Yih Tang.  This is a " << endl
            << " program developed based on the concept and basic rules of UNO for education " << endl
            << " purpose.  This is a NON-COMMERCIAL PROGRAM owned by Yeo Yih Tang" << endl << endl;
        cout << " UNO is a registered trademark under Mattel Corp." << endl << endl;


        backtomenu = false;
        cout << "Welcome!" << endl << endl;
        cout << "Please select from the menu:" << endl
            << "1\tStart Game" << endl
            << "2\tTutorial (Standard Rule)" << endl
            << "3\tAbout" << endl
            << "0\tExit" << endl;
        do
        {
            reply = 0;
            repeatchoose = false;

            cout << "Choice: ";
            cin.getline(replych,100);

            if (strlen(replych)==0)
            {
                cout << "Error: Please enter something!" << endl;
                cin.clear();
                repeatchoose = true;
            }

            if (repeatchoose == false)
            {
                for (int checkint = 0; checkint < strlen(replych); checkint++)
                {
                    if (replych[checkint] != '1' &&
                        replych[checkint] != '2' &&
                        replych[checkint] != '3' &&
                        replych[checkint] != '4' &&
                        replych[checkint] != '5' &&
                        replych[checkint] != '6' &&
                        replych[checkint] != '7' &&
                        replych[checkint] != '8' &&
                        replych[checkint] != '9' &&
                        replych[checkint] != '0')
                    {
                        cout << "Error! Enter only numbers!" << endl;
                        cin.clear();
                        repeatchoose = true;
                        break;
                    }
                    else
                        repeatchoose = false;
                }

                if (repeatchoose == false)
                {
                    for (int counter = 0; counter < strlen(replych); counter++)
                    {
                        switch (replych[counter])
                        {
                            case '1':
                            numpower = 1;
                            break;

                            case '2':
                            numpower = 2;
                            break;

                            case '3':
                            numpower = 3;
                            break;

                            case '4':
                            numpower = 4;
                            break;

                            case '5':
                            numpower = 5;
                            break;

                            case '6':
                            numpower = 6;
                            break;

                            case '7':
                            numpower = 7;
                            break;

                            case '8':
                            numpower = 8;
                            break;

                            case '9':
                            numpower = 9;
                            break;

                            case '0':
                            numpower = 0;
                            break;

                            default: // impossible to reach here
                            numpower = 0;
                            break;
                        }
                        powerresult = findpower(strlen(replych)-counter-1);
                        reply += powerresult*numpower;
                    }

                    if (reply < 0 || reply > 3)
                    {
                        cout << "Error: Enter valid number only" << endl;
                        cin.clear();
                        repeatchoose = true;
                    }
                }
            }
            if (repeatchoose)
                cout << endl;
        }while (repeatchoose);
        if (reply == 0)
        {
            exitprog = true;
            system("cls");
        }

        if (exitprog != true)
        {

            if (reply == 2)
            {
                system("cls");
                cout << "          ============== UNO v1.0 TUTORIAL ================ " << endl;
                cout << "\nPlease be informed that this program is developed based on the standard " << endl
                    << "rules of UNO Game.  There are a few more rules, namely Penalty Rule, 7-0 Rule" << endl
                    << "and Jump-In rule, which is NOT developed under this version 1.0.  Those may be" << endl
                    << "considered to be incorporated into version 2.0.  So stay tuned for the updates" << endl << endl;
                cout << "Rules:" << endl
                    << "1. Play a card by entering the number on the left of the card." << endl
                    << "2. A card can only be played if the colour or the title of the card is same " << endl
                    << "   as the card on the pile." << endl
                    << "3. Draw a card if you cannot play any card, or you want to have more cards just" << endl
                    << "   for fun." << endl
                    << "4. The last card in your hand can be a power card." << endl
                    << "5. Draw Two and Draw Four will cause the next user to be skipped." << endl
                    << "6. Wild card can be played at any time without restriction, but for Draw Four," << endl
                    << "   the computer players might challenge you.  Draw Four can only be played when" << endl
                    << "   you have no same colour/ title card in your hand.  If you are found guilty," << endl
                    << "   you will draw 4 cards as punishment, but you still can choose your colour." << endl
                    << "   If challenge failed, the challenger will draw 6 cards instead of 4." << endl
                    << "7. A game will end, when one of the player had finished all the cards in his " << endl
                    << "   hand, or when the cards had ran out of stock." << endl;
                cout << "\nIf you experience any problem or found any bug, please report to me at: " << endl
                    << "Email: yeoyihtang@live.com" << endl << endl;

                system("pause");
                backtomenu = true;
                system("cls");
            }

            else if (reply == 3)
            {
                system("cls");
                cout << "          ============== ABOUT UNO v1.0 ================ " << endl;
                cout << "\nBuild information: version 1.0 RC build 220" << endl
                    << "\nDeveloper: Yeo Yih Tang" << endl
                    << "Bug Reporter: Kin Gwn, Steven Ong" << endl
                    << "Alpha tester: Kin Gwn, Steven Ong, Cher Seng, Xin Dee" << endl
                    << "Beta tester: Kin Gwn, Jian Shen, Xin Dee" << endl << endl;
                cout << "Contribute to this program and get listed here!" << endl << endl;
                system("pause");
                backtomenu = true;
                system("cls");
            }

            else if (reply == 1)
            {
                do
                {
                    repeatgame = false;
                    distrindex = 0; // reset, fix bug unable to replay if card ran out
                    system("color F0");
                    system("cls");
                    win = false;
                    lose = false;
                    cout << endl;
                    cout << "    oooo   o    oooo    oooo    ooo   oooo     o  o  o    oooo    ooo   ooooo " << endl
                         << "    o   o  o    o      o    o  o      o        o  o  o   o    o    o      o   " << endl
                         << "    oooo   o    oooo   oooooo   ooo   oooo     o  o  o   oooooo    o      o   " << endl
                         << "    o      o    o      o    o      o  o        o  o  o   o    o    o      o   " << endl
                         << "    o      oooo oooo   o    o   ooo   oooo      oo oo    o    o   ooo     o   " << endl << endl;
                    cout << "          UNO Program version 1.0 developed by YEO YIH TANG, year 2010" << endl << endl;
                    cout << "Loading..";
                    randomsort(card);

                    for (int i0 = 0; i0 < 4; i0++)
                    {
                        for (int i1 = 0; i1 < 7; i1++)
                        {
                            strcpy (p[i0].playcard[i1].name, card[distrindex].name);
                            strcpy (p[i0].playcard[i1].colour, card[distrindex].colour);
                            distrindex++;
                        }
                        p[i0].num_card = 7;
                    }

                    bool repeatcarddraw;
                    do
                    {
                        repeatcarddraw = false;
                        strcpy(cardontable.name,card[distrindex].name);
                        strcpy(cardontable.colour,card[distrindex].colour);
                        distrindex++;

                        if (strcmpi (cardontable.name, "draw four") == 0 ||
                            strcmpi (cardontable.name, "draw two") == 0 ||
                            strcmpi (cardontable.name, "wild") == 0 ||
                            strcmpi (cardontable.name, "skip") == 0 ||
                            strcmpi (cardontable.name, "reverse") == 0 )
                        {
                            repeatcarddraw = true;
                        }
                    }while (repeatcarddraw);

                    int token = 1600; // to determine which guy to play first, 800 to ensure the card will not finish even if counterclockwise
                    bool clockwise = true; // to check reverse or not

                    while (win != true && lose != true)
                    {
                        if (distrindex >= 108)
                            break;

                        if (p[0].num_card == 0)
                        {
                            win = true;
                            break;
                        }
                        else if (p[1].num_card == 0 || p[2].num_card == 0 || p[3].num_card == 0)
                        {
                            lose = true;
                            break;
                        }

                            system ("cls");

                            //// border line #1 ////
                            for (int space = 0; space < 15; space++)
                                cout << " ";
                            for (int border = 0; border < 14; border++)
                                cout << "=";
                            cout << " ";
                            for (int border = 0; border < 14; border++)
                                cout << "=";
                            cout << endl;

                            //// line #1 ////
                            for (int space = 0; space < 15; space++)
                                cout << " ";
                            cout << ((strcmpi(cardontable.name," ") == 0)? "Colour chosen:": "Card on table:");
                            for (int space = 0; space < 1; space++)
                                cout << " ";
                            cout << "Cards left:" << endl;

                            //// line #2 ////
                            for (int space = 0; space < 15; space++)
                                cout << " ";
                            if (strcmpi(cardontable.colour,"red") == 0)
                                system("color F4");
                            else if (strcmpi(cardontable.colour, "yellow") == 0)
                                system("color F6");
                            else if (strcmpi(cardontable.colour, "blue") == 0)
                                system("color F1");
                            else if (strcmpi(cardontable.colour, "green") == 0)
                                system("color F2");
                            strupr(cardontable.colour);
                            cout << cardontable.colour;
                            for (int space = 0; space < (15-strlen(cardontable.colour)); space++)
                                cout << " ";
                            cout << 108-distrindex << endl;

                            //// line #3 ////
                            for (int space = 0; space < 15; space++)
                                cout << " ";
                            cout << ((strcmpi(cardontable.name," ") == 0)? "-": cardontable.name);
                            for (int space = 0; space < (15-strlen(cardontable.name)); space++)
                                cout << " ";
                            cout << "[Direction: " << (clockwise? ">>" : "<<") << "]";
                            cout << endl;

                            //// border line #2 ////
                            for (int space = 0; space < 15; space++)
                                cout << " ";
                            for (int border = 0; border < 14; border++)
                                cout << "=";
                            cout << " ";
                            for (int border = 0; border < 14; border++)
                                cout << "=";
                            cout << endl;

                            /////////////////////////////////////////////////////////

                            //// border line #3 ////
                            for (int border = 0; border < 14; border++)
                                cout << "=";
                            cout << " ";
                            for (int border = 0; border < 14; border++)
                                cout << "=";
                            cout << " ";
                            for (int border = 0; border < 14; border++)
                                cout << "=";
                            cout << " ";
                            for (int border = 0; border < 14; border++)
                                cout << "=";
                            cout << endl;

                            //// line #1 ////
                            cout << "YOU:";
                            for (int space = 0; space < 11; space++)
                                cout << " ";
                            cout << "Player #2:";
                            for (int space = 0; space < 5; space++)
                                cout << " ";
                            cout << "Player #3:";
                            for (int space = 0; space < 5; space++)
                                cout << " ";
                            cout << "Player #4:";
                            for (int space = 0; space < 5; space++)
                                cout << " ";
                            cout << endl;

                            //// line #2 ////
                            cout << "Cards: " << p[0].num_card;
                            for (int space = 0; space < (p[0].num_card>9? 6:7); space++)
                                cout << " ";
                            cout << "Cards: " << p[1].num_card;
                            for (int space = 0; space < (p[1].num_card>9? 6:7); space++)
                                cout << " ";
                            cout << "Cards: " << p[2].num_card;
                            for (int space = 0; space < (p[2].num_card>9? 6:7); space++)
                                cout << " ";
                            cout << "Cards: " << p[3].num_card;
                            for (int space = 0; space < (p[3].num_card>9? 6:7); space++)
                                cout << " ";
                            cout << endl;

                            //// line #3 ////
                            cout << (p[0].num_card == 1? "UNO!" : "");
                            for (int space = 0; space < (p[0].num_card == 1? 11:15); space++)
                                cout << " ";
                            cout << (p[1].num_card == 1? "UNO!" : "");
                            for (int space = 0; space < (p[1].num_card == 1? 11:15); space++)
                                cout << " ";
                            cout << (p[2].num_card == 1? "UNO!" : "");
                            for (int space = 0; space < (p[2].num_card == 1? 11:15); space++)
                                cout << " ";
                            cout << (p[3].num_card == 1? "UNO!" : "");
                            for (int space = 0; space < (p[3].num_card == 1? 11:15); space++)
                                cout << " ";
                            cout << endl;

                            //// border line #4 ////
                            for (int border = 0; border < 14; border++)
                                cout << "=";
                            cout << " ";
                            for (int border = 0; border < 14; border++)
                                cout << "=";
                            cout << " ";
                            for (int border = 0; border < 14; border++)
                                cout << "=";
                            cout << " ";
                            for (int border = 0; border < 14; border++)
                                cout << "=";
                            cout << endl;


                        // HUMAN PLAYER //////////////////////////////////////////////////////////////////////

                        if (token % 4 == 0)
                        {
                            coloursort(p);
                            displaycard(p[0], p[0].num_card);
                            do
                            {
                                playsuccess = false;
                                putfail = false;
                                dontproceed = false;

                                //////////////////////////////////////////////////////////////////////////

                                do
                                {
                                    card_choice = 0;
                                    repeatchoose2 = false;
                                    cout << "\nChoose a card to play [0 to draw a card]: ";
                                    cin.getline (replych2,100);

                                    if (strlen(replych2)==0)
                                    {
                                        cout << "Error! Enter something please" << endl;
                                        cin.clear();
                                        repeatchoose2 = true;
                                    }


                                    if (repeatchoose2 == false)
                                    {
                                        for (int checkint = 0; checkint < strlen(replych); checkint++)
                                        {
                                            if (replych2[checkint] != '1' &&
                                                replych2[checkint] != '2' &&
                                                replych2[checkint] != '3' &&
                                                replych2[checkint] != '4' &&
                                                replych2[checkint] != '5' &&
                                                replych2[checkint] != '6' &&
                                                replych2[checkint] != '7' &&
                                                replych2[checkint] != '8' &&
                                                replych2[checkint] != '9' &&
                                                replych2[checkint] != '0')
                                            {
                                                cout << "Error! Enter only numbers!" << endl;
                                                repeatchoose2 = true;
                                                break;
                                            }
                                            else
                                                repeatchoose2 = false;
                                        }

                                        if (repeatchoose2 == false)
                                        {
                                            for (int counter = 0; counter < strlen(replych2); counter++)
                                            {
                                                switch (replych2[counter])
                                                {
                                                    case '1':
                                                    numpower2 = 1;
                                                    break;

                                                    case '2':
                                                    numpower2 = 2;
                                                    break;

                                                    case '3':
                                                    numpower2 = 3;
                                                    break;

                                                    case '4':
                                                    numpower2 = 4;
                                                    break;

                                                    case '5':
                                                    numpower2 = 5;
                                                    break;

                                                    case '6':
                                                    numpower2 = 6;
                                                    break;

                                                    case '7':
                                                    numpower2 = 7;
                                                    break;

                                                    case '8':
                                                    numpower2 = 8;
                                                    break;

                                                    case '9':
                                                    numpower2 = 9;
                                                    break;

                                                    case '0':
                                                    numpower2 = 0;
                                                    break;

                                                    default: // impossible to reach here
                                                    numpower2 = 0;
                                                    break;
                                                }
                                                powerresult2 = findpower(strlen(replych2)-counter-1);
                                                card_choice += powerresult2*numpower2;
                                            }

                                            card_choice -= 1; // because in real case 1 = 0; 0 = -1; 2 =1; (in array notation)

                                            if (card_choice < -1 || card_choice > p[0].num_card-1) // note: num must minus 1 because the term appear to the user is more than the actual index by 1
                                            {
                                                cout << "Error: Enter valid number only" << endl;
                                                cin.clear();
                                                repeatchoose2 = true;
                                            }
                                        }
                                    }
                                } while (repeatchoose2);


                                //////////////////////////////////////////////////////////////////////////

                                if (card_choice == -1)
                                {
                                    drawcard (card, p, 0, cardontable.name, cardontable.colour, playsuccess);
                                }
                                else
                                {
                                    strcpy(previouscard.colour, cardontable.colour);
                                    strcpy(previouscard.name, cardontable.name);
                                    putcard (p[0], p[0].num_card, card_choice, cardontable.name, cardontable.colour, putfail);

                                    if (putfail == true)
                                    {
                                        cout << "Error: You are not allowed to play this card" << endl;
                                        playsuccess = false;
                                    }
                                    else if (putfail == false)
                                    {
                                        swapcard(card_choice, p, 0, p[0].num_card);
                                        playsuccess = true;
                                    }
                                }

                            } while (putfail == true);


                            if (strcmpi(cardontable.name, "draw four") == 0 && playsuccess == true)
                            {
                                if (clockwise)
                                {
                                    drawTwoVictim = 1;
                                    token++; // skip the victim haha
                                }
                                else if (clockwise == false)
                                {
                                    drawTwoVictim = 3;
                                    token--; // skip the victim haha
                                }
                                drawFour (card, p, drawTwoVictim, clockwise, previouscard, token);
                                playsuccess = false;
                            }

                            if (strcmpi (cardontable.colour, "black") == 0)
                            {
                                chooseColour (cardontable.colour, cardontable.name, 0, p[0]);
                            }

                            if ((strcmpi(cardontable.name, "draw two")) == 0 && (playsuccess == true))
                            {
                                if (clockwise)
                                {
                                    drawTwoVictim = 1;
                                    token++; // skip the victim haha
                                }
                                else if (clockwise == false)
                                {
                                    drawTwoVictim = 3;
                                    token--; // skip the victim haha
                                }
                                drawTwo (card, p, drawTwoVictim);
                                playsuccess = false;
                            }

                            if (strcmpi (cardontable.name, "skip") == 0 && playsuccess == true)
                            {
                                if (clockwise)
                                    token++;
                                else if (clockwise == false)
                                    token--;
                                playsuccess = false;
                            }
                            if (strcmpi (cardontable.name, "reverse") == 0 && playsuccess == true)
                            {
                                if (clockwise && dontproceed == false)
                                {
                                    clockwise = false;
                                    dontproceed = true; // do not continue to the statement clockwise == false, or else the reverse will not function
                                }
                                else if (clockwise == false && dontproceed == false)
                                {
                                    clockwise = true;
                                    dontproceed = true;
                                }
                                playsuccess = false;
                            }

                            if (clockwise)
                                token++;

                            else if (clockwise == false)
                                token--;

                            cout << "[Wait...]";
                            time_t start_time, cur_time;
                            time(&start_time);
                            do
                            {
                                time(&cur_time);
                            }while((cur_time - start_time) < 2);
                            system("cls");
                        }

                        // redundant coding t o fix bug: delay in showing win or lose

                        if (p[0].num_card == 0)
                        {
                            win = true;
                            break;
                        }
                        else if (p[1].num_card == 0 || p[2].num_card == 0 || p[3].num_card == 0)
                        {
                            lose = true;
                            break;
                        }

                            system ("cls");

                            //// border line #1 ////
                            for (int space = 0; space < 15; space++)
                                cout << " ";
                            for (int border = 0; border < 14; border++)
                                cout << "=";
                            cout << " ";
                            for (int border = 0; border < 14; border++)
                                cout << "=";
                            cout << endl;

                            //// line #1 ////
                            for (int space = 0; space < 15; space++)
                                cout << " ";
                            cout << ((strcmpi(cardontable.name," ") == 0)? "Colour chosen:": "Card on table:");
                            for (int space = 0; space < 1; space++)
                                cout << " ";
                            cout << "Cards left:" << endl;

                            //// line #2 ////
                            for (int space = 0; space < 15; space++)
                                cout << " ";
                            if (strcmpi(cardontable.colour,"red") == 0)
                                system("color F4");
                            else if (strcmpi(cardontable.colour, "yellow") == 0)
                                system("color F6");
                            else if (strcmpi(cardontable.colour, "blue") == 0)
                                system("color F1");
                            else if (strcmpi(cardontable.colour, "green") == 0)
                                system("color F2");
                            strupr(cardontable.colour);
                            cout << cardontable.colour;
                            for (int space = 0; space < (15-strlen(cardontable.colour)); space++)
                                cout << " ";
                            cout << 108-distrindex << endl;

                            //// line #3 ////
                            for (int space = 0; space < 15; space++)
                                cout << " ";
                            cout << ((strcmpi(cardontable.name," ") == 0)? "-": cardontable.name);
                            for (int space = 0; space < (15-strlen(cardontable.name)); space++)
                                cout << " ";
                            cout << "[Direction: " << (clockwise? ">>" : "<<") << "]";
                            cout << endl;

                            //// border line #2 ////
                            for (int space = 0; space < 15; space++)
                                cout << " ";
                            for (int border = 0; border < 14; border++)
                                cout << "=";
                            cout << " ";
                            for (int border = 0; border < 14; border++)
                                cout << "=";
                            cout << endl;

                            /////////////////////////////////////////////////////////

                            //// border line #3 ////
                            for (int border = 0; border < 14; border++)
                                cout << "=";
                            cout << " ";
                            for (int border = 0; border < 14; border++)
                                cout << "=";
                            cout << " ";
                            for (int border = 0; border < 14; border++)
                                cout << "=";
                            cout << " ";
                            for (int border = 0; border < 14; border++)
                                cout << "=";
                            cout << endl;

                            //// line #1 ////
                            cout << "YOU:";
                            for (int space = 0; space < 11; space++)
                                cout << " ";
                            cout << "Player #2:";
                            for (int space = 0; space < 5; space++)
                                cout << " ";
                            cout << "Player #3:";
                            for (int space = 0; space < 5; space++)
                                cout << " ";
                            cout << "Player #4:";
                            for (int space = 0; space < 5; space++)
                                cout << " ";
                            cout << endl;

                            //// line #2 ////
                            cout << "Cards: " << p[0].num_card;
                            for (int space = 0; space < (p[0].num_card>9? 6:7); space++)
                                cout << " ";
                            cout << "Cards: " << p[1].num_card;
                            for (int space = 0; space < (p[1].num_card>9? 6:7); space++)
                                cout << " ";
                            cout << "Cards: " << p[2].num_card;
                            for (int space = 0; space < (p[2].num_card>9? 6:7); space++)
                                cout << " ";
                            cout << "Cards: " << p[3].num_card;
                            for (int space = 0; space < (p[3].num_card>9? 6:7); space++)
                                cout << " ";
                            cout << endl;

                            //// line #3 ////
                            cout << (p[0].num_card == 1? "UNO!" : "");
                            for (int space = 0; space < (p[0].num_card == 1? 11:15); space++)
                                cout << " ";
                            cout << (p[1].num_card == 1? "UNO!" : "");
                            for (int space = 0; space < (p[1].num_card == 1? 11:15); space++)
                                cout << " ";
                            cout << (p[2].num_card == 1? "UNO!" : "");
                            for (int space = 0; space < (p[2].num_card == 1? 11:15); space++)
                                cout << " ";
                            cout << (p[3].num_card == 1? "UNO!" : "");
                            for (int space = 0; space < (p[3].num_card == 1? 11:15); space++)
                                cout << " ";
                            cout << endl;

                            //// border line #4 ////
                            for (int border = 0; border < 14; border++)
                                cout << "=";
                            cout << " ";
                            for (int border = 0; border < 14; border++)
                                cout << "=";
                            cout << " ";
                            for (int border = 0; border < 14; border++)
                                cout << "=";
                            cout << " ";
                            for (int border = 0; border < 14; border++)
                                cout << "=";
                            cout << endl;



                        // COMPUTER PLAYERs ////////////////////////////////////////////////////////////////////

                        if (distrindex >= 108)
                            break;

                        if (token % 4 != 0)
                        {
                            playsuccess = false;

                            if (token % 4 == 1)
                                playernum = 1;
                            else if (token % 4 == 2)
                                playernum = 2;
                            else if (token % 4 == 3)
                                playernum = 3;

                            bool compplay = false;
                            dontproceed = false;
                            blackplay = false;
                            //cout << "Now is PLAYER " << playernum +1 <<"'s turn" << endl << endl;
                            for (int playerplay = 0; playerplay < p[playernum].num_card; playerplay++)
                            {
                                if (strcmpi(p[playernum].playcard[playerplay].colour, cardontable.colour)==0 ||
                                    strcmpi(p[playernum].playcard[playerplay].name, cardontable.name) == 0)
                                {
                                    compplay = true;
                                    card_choice = playerplay;
                                    strcpy(cardontable.colour, p[playernum].playcard[playerplay].colour);
                                    strcpy(cardontable.name, p[playernum].playcard[playerplay].name);
                                    playsuccess = true;
                                    for (int space = 0; space < playernum*15; space++)
                                        cout << " ";
                                    strupr(p[playernum].playcard[playerplay].colour);
                                    cout  << p[playernum].playcard[playerplay].colour << " " << p[playernum].playcard[playerplay].name << " played" << endl;
                                    swapcard(card_choice, p, playernum, p[playernum].num_card);
                                    p[playernum].num_card--;
                                    break;
                                }
                                else
                                {
                                    compplay = false;
                                    playsuccess = false;
                                }
                            }

                            if (compplay == false)
                            {
                                for (int playerplay = 0; playerplay < p[playernum].num_card; playerplay++)
                                {
                                    if (strcmpi(p[playernum].playcard[playerplay].colour,"black") == 0)
                                    {
                                        compplay = true;
                                        blackplay = true;
                                        card_choice = playerplay;
                                        strcpy(previouscard.colour, cardontable.colour);
                                        strcpy(previouscard.name, cardontable.name);
                                        strcpy(cardontable.colour, p[playernum].playcard[playerplay].colour);
                                        strcpy(cardontable.name, p[playernum].playcard[playerplay].name);
                                        playsuccess = true;
                                        for (int space = 0; space < playernum*15; space++)
                                            cout << " ";
                                        strupr(p[playernum].playcard[playerplay].colour);
                                        cout  << p[playernum].playcard[playerplay].colour << " " << p[playernum].playcard[playerplay].name << " played" << endl;
                                        chooseColour (cardontable.colour, cardontable.name, playernum, p[playernum]);

                                        if (strcmpi(p[playernum].playcard[playerplay].name,"draw four") == 0 && playsuccess == true)
                                        {
                                            if (clockwise)
                                            {
                                                if (playernum == 1)
                                                    drawTwoVictim = 2;
                                                else if (playernum == 2)
                                                    drawTwoVictim = 3;
                                                else if (playernum == 3)
                                                    drawTwoVictim = 0;
                                                token++; // skip the victim haha
                                            }
                                            else if (clockwise == false)
                                            {
                                                if (playernum == 1)
                                                    drawTwoVictim = 0;
                                                else if (playernum == 2)
                                                    drawTwoVictim = 1;
                                                else if (playernum == 3)
                                                    drawTwoVictim = 2;
                                                token--; // skip the victim haha
                                            }
                                            drawFour (card, p, drawTwoVictim, clockwise, previouscard, token);
                                        }

                                        swapcard(card_choice, p, playernum, p[playernum].num_card);
                                        p[playernum].num_card--;
                                        break;
                                    }
                                    else
                                    {
                                        compplay = false;
                                        playsuccess = false;
                                    }
                                }

                            }

                            if (compplay == false)
                            {
                                playsuccess = false;
                                for (int space = 0; space < playernum*15; space++)
                                        cout << " ";
                                cout <<"Draw 1 card" << endl;
                                drawcard (card, p, playernum, cardontable.name, cardontable.colour, playsuccess);
                                // NOTE THIS DOES NOT INCLUDE THE FEATURE OF DRAW FOUR!!!
                            }

                            if ((strcmpi(cardontable.name, "draw two") == 0) && (playsuccess == true))
                            {
                                if (clockwise)
                                {
                                    if (playernum == 1)
                                        drawTwoVictim = 2;
                                    else if (playernum == 2)
                                        drawTwoVictim = 3;
                                    else if (playernum == 3)
                                        drawTwoVictim = 0;
                                    token++; // skip the victim haha
                                }
                                else if (clockwise == false)
                                {
                                    if (playernum == 1)
                                        drawTwoVictim = 0;
                                    else if (playernum == 2)
                                        drawTwoVictim = 1;
                                    else if (playernum == 3)
                                        drawTwoVictim = 2;
                                    token--; // skip the victim haha
                                }
                                drawTwo (card, p, drawTwoVictim);
                                playsuccess = false;
                            }

                            if (strcmpi (cardontable.name, "skip") == 0 && playsuccess == true)
                            {
                                if (clockwise)
                                    token++;
                                else if (clockwise == false)
                                    token--;
                                playsuccess = false;
                            }

                            if (strcmpi (cardontable.name, "reverse") == 0 && playsuccess == true)
                            {
                                if (clockwise && dontproceed == false)
                                {
                                    clockwise = false;
                                    dontproceed = true; // do not continue to the statement clockwise == false, or else the reverse will not function
                                }
                                else if (clockwise == false && dontproceed == false)
                                {
                                    clockwise = true;
                                    dontproceed = true;
                                }
                                playsuccess = false;
                            }

                            if (clockwise)
                                token++;

                            else if (clockwise == false)
                                token--;

                            cout << "[Wait...]";
                            time_t start_time, cur_time;
                            time(&start_time);
                            do
                            {
                                time(&cur_time);
                            }while((cur_time - start_time) < 2);
                            system("cls");
                        }
                        // END OF COMPUTER PLAYERS//////////////////////////////////////////////////////////////////////////

                    }

                    system("cls");
                    system("color F0");

                    if (win)
                    {
                        cout << "    oooo   ooo   oo  o   oooo  oooo    ooo   ooooo  ooooo " << endl
                             << "   o      o   o  oo  o  o      o   o  o   o    o       o  " << endl
                             << "   o      o   o  o o o  o  oo  oooo   ooooo    o      o   " << endl
                             << "   o      o   o  o  oo  o   o  o  o   o   o    o     o    " << endl
                             << "    oooo   ooo   o  oo   oooo  o   o  o   o    o    ooooo " << endl << endl;
                        cout << "            CONGRATULATION!! YOU WON THIS GAME" << endl;
                    }
                    else if (lose)
                    {
                        cout << "             ooooo     oooo     oooo   " << endl
                             << "             o    o   o    o   o    o  " << endl
                             << "             ooooo    o    o   o    o  " << endl
                             << "             o    o   o    o   o    o  " << endl
                             << "             ooooo     oooo     oooo   " << endl << endl;
                        cout << "            YOU LOSE!! =( Try Again!" << endl;
                    }
                    else
                    {
                        cout << "          oooo   oo  oo     ooo  oo   oooo   " << endl
                             << "         o    o  oo  oo     ooo  oo  o    o " << endl
                             << "         o    o  oooooo     oo o oo  o    o " << endl
                             << "         o    o  oo  oo     oo  ooo  o    o " << endl
                             << "          oooo   oo  oo     oo  ooo   oooo  " << endl << endl;
                        cout << "The game has been terminated because the cards ran out.. =(" << endl;
                    }

                    cout << "\nDo you want to play again? (Y/N): ";
                    cin.getline(userans,100);

                    if (strcmpi(userans,"y") != 0)
                    {
                        cin.clear();
                        exitprog = true;
                    }
                    else
                    {
                        cin.clear();
                        repeatgame = true;
                    }
                }while (repeatgame);

            }
        }
    }while (backtomenu && exitprog != true);
    system("cls");
    cout << endl;
    cout << "         ooooo    oo    oo   ooooo    0  " << endl
         << "         o    o    oo  oo    oo       0  " << endl
         << "         ooooo      oooo     ooooo    0  " << endl
         << "         o    o      oo      oo          " << endl
         << "         ooooo       oo      ooooo    0  " << endl << endl;
    cout << "           Have a nice day, bye!" << endl << endl << endl;
    cout << "All right reserved by YEO YIH TANG, year 2010.  UNO is a trademark belonged" << endl
        << "to Mattel Corporation.  This program is not for commercial use." << endl << endl;

    system ("pause");



    return 0;
}



void displaycard (player yourself, int num)
{
    cout << "\nYour cards are as below: " << endl;
    for (int displaycard = 0; displaycard < yourself.num_card; displaycard++)
    {
        cout << displaycard+1 << "\t[" << yourself.playcard[displaycard].colour << "] [" << yourself.playcard[displaycard].name << "]" << endl;
    }
}



void swapcard (int choice, player p[], int index, int num)
{
    if (choice != num)
    {
        strcpy(p[index].playcard[choice].colour, p[index].playcard[choice+1].colour);
        strcpy(p[index].playcard[choice].name, p[index].playcard[choice+1].name);
        swapcard(choice+1, p, index, num);
    }
}

void drawcard(unocard card[], player p[], int index, char cardontablename[], char cardontablecolour[], bool &playsuccess)
{
    int randnum, delaytime;
    bool repeatrandgen, putfail = false, repeat;
    int reply, card_choice;
    char replych[100];
    int numpower, powerresult;

    strcpy(p[index].playcard[p[index].num_card].name,card[distrindex].name);
    strcpy(p[index].playcard[p[index].num_card].colour,card[distrindex].colour);
    distrindex++;
    p[index].num_card++;

    if (index == 0)
    {
        cout << "You get this card: [" << p[0].playcard[(p[0].num_card-1)].colour << "] [" << p[0].playcard[(p[0].num_card-1)].name << "]" << endl;
        // ask whether to play that card!
        if (strcmpi(p[index].playcard[p[index].num_card-1].colour, cardontablecolour) == 0 ||
            strcmpi(p[index].playcard[p[index].num_card-1].name, cardontablename) == 0 ||
            strcmpi(p[index].playcard[p[index].num_card-1].colour, "Black") == 0)
        {

            do
            {
                repeat = false;
                reply = 0;
                cout << "\nDo you want to play this card? (1 YES, 2 NO): ";
                cin.getline(replych,100);

                if (strlen(replych)== 0)
                {
                    cout << "Error! Enter something please" << endl;
                    cin.clear();
                    repeat = true;
                }

                for (int checkint = 0; checkint < strlen(replych); checkint++)
                {
                    if (replych[checkint] != '1' &&
                        replych[checkint] != '2' &&
                        replych[checkint] != '3' &&
                        replych[checkint] != '4' &&
                        replych[checkint] != '5' &&
                        replych[checkint] != '6' &&
                        replych[checkint] != '7' &&
                        replych[checkint] != '8' &&
                        replych[checkint] != '9' &&
                        replych[checkint] != '0')
                    {
                        cout << "Error! Enter only numbers!" << endl;
                        cin.clear();
                        repeat = true;
                        break;
                    }
                    else
                        repeat = false;
                }

                if (repeat == false)
                {
                    for (int counter = 0; counter < strlen(replych); counter++)
                    {
                        switch (replych[counter])
                        {
                            case '1':
                            numpower = 1;
                            break;

                            case '2':
                            numpower = 2;
                            break;

                            case '3':
                            numpower = 3;
                            break;

                            case '4':
                            numpower = 4;
                            break;

                            case '5':
                            numpower = 5;
                            break;

                            case '6':
                            numpower = 6;
                            break;

                            case '7':
                            numpower = 7;
                            break;

                            case '8':
                            numpower = 8;
                            break;

                            case '9':
                            numpower = 9;
                            break;

                            case '0':
                            numpower = 0;
                            break;

                            default: // impossible to reach here
                            numpower = 0;
                            break;
                        }
                        powerresult = findpower(strlen(replych)-counter-1);
                        reply += powerresult*numpower;

                    }
                    if (reply == 1)
                    {
                        card_choice = p[index].num_card-1;
                        repeat = false;
                        playsuccess = true;
                        putcard (p[index], p[index].num_card, card_choice, cardontablename, cardontablecolour, putfail);
                    }
                    else if (reply != 1 && reply != 2)
                    {
                        cout << "Error! Enter 1 or 2 only" << endl;
                        cin.clear();
                        repeat = true;
                    }
                }
            }while (repeat);
        }
    }
    else if (index != 0)
    {
        if (strcmpi(p[index].playcard[p[index].num_card-1].colour, cardontablecolour) == 0 ||
            strcmpi(p[index].playcard[p[index].num_card-1].name, cardontablename) == 0)
        {
            card_choice = p[index].num_card-1;
            putcard (p[index], p[index].num_card, card_choice, cardontablename, cardontablecolour, putfail);
            for (int space = 0; space < index*15; space++)
                cout << " ";
            strupr(p[index].playcard[p[index].num_card].colour);
            cout  << p[index].playcard[p[index].num_card].colour << " "<< p[index].playcard[p[index].num_card].name << " played" << endl;
            playsuccess = true;
        }
        else
        {
            for (int space = 0; space < index*15; space++)
                cout << " ";
            cout << "Card not played" << endl;
            playsuccess = false;
        }
    }
}

void putcard (player yourself, int &num, int card_choice, char cardontablename[], char cardontablecolour[], bool &putfail)
{
    if (strcmpi(yourself.playcard[card_choice].colour, cardontablecolour) == 0 ||
        strcmpi(yourself.playcard[card_choice].name, cardontablename) == 0)
    {
        num--;
        strcpy(cardontablename, yourself.playcard[card_choice].name);
        strcpy(cardontablecolour,yourself.playcard[card_choice].colour);
        putfail = false;
    }
    else
    {
        if (strcmpi(yourself.playcard[card_choice].colour, "Black") == 0)
        {
            num--;
            strcpy(cardontablecolour, yourself.playcard[card_choice].colour);
            strcpy(cardontablename, yourself.playcard[card_choice].name);
            putfail = false;
            //chooseColour (cardontablecolour, cardontablename, 0);
        }
        else
            putfail = true;
    }
}

void drawTwo (unocard card[], player p[], int victim)
{
    bool repeatrandgen;
    for (int draw = 0; draw < 2; draw++)
    {
        strcpy (p[victim].playcard[p[victim].num_card].name, card[distrindex].name);
        strcpy (p[victim].playcard[p[victim].num_card].colour, card[distrindex].colour);
        distrindex++;
        p[victim].num_card += 1;
    }
    if (victim == 0)
    {
        cout << "[DRAW TWO ON YOU!!]" << endl;
        cout << "You get:" << endl;
        cout << "[" << p[victim].playcard[p[victim].num_card - 2].colour << "] [" << p[victim].playcard[p[victim].num_card - 2].name << "]" << endl;
        cout << "[" << p[victim].playcard[p[victim].num_card - 1].colour << "] [" << p[victim].playcard[p[victim].num_card - 1].name << "]" << endl;
    }
    else
    {
        cout << "Player " << victim+1 << " draws 2 cards" << endl;
    }
}

void drawFour (unocard card[], player p[], int victim, bool clockwise, unocard previouscard, int &token)
{
    int delaytime, randnum;
    bool repeatrandgen;
    int reply;
    char replych[100];
    bool challengesuccess, repeat;
    int beingdraw;
    int challenged;
    int numpower, powerresult;
    int drawq;

    if (victim == 0)
    {
        cout << "Challenge or Draw?" << endl;
        cout << "1\tChallenge" << endl
            << "2\tDraw" << endl;
        do
        {
            repeat = false;
            reply = 0;
            cout << "Reply: ";
            cin.getline(replych,100);


            if (strlen(replych)== 0)
            {
                cout << "Error! Enter something please" << endl;
                cin.clear();
                repeat = true;
            }


            for (int checkint = 0; checkint < strlen(replych); checkint++)
            {
                if (replych[checkint] != '1' &&
                    replych[checkint] != '2' &&
                    replych[checkint] != '3' &&
                    replych[checkint] != '4' &&
                    replych[checkint] != '5' &&
                    replych[checkint] != '6' &&
                    replych[checkint] != '7' &&
                    replych[checkint] != '8' &&
                    replych[checkint] != '9' &&
                    replych[checkint] != '0')
                {
                    cout << "Error! Enter only numbers!" << endl;
                    cin.clear();
                    repeat = true;
                    break;
                }
                else
                    repeat = false;
            }

            if (repeat == false)
            {
                for (int counter = 0; counter < strlen(replych); counter++)
                {
                    switch (replych[counter])
                    {
                        case '1':
                        numpower = 1;
                        break;

                        case '2':
                        numpower = 2;
                        break;

                        case '3':
                        numpower = 3;
                        break;

                        case '4':
                        numpower = 4;
                        break;

                        case '5':
                        numpower = 5;
                        break;

                        case '6':
                        numpower = 6;
                        break;

                        case '7':
                        numpower = 7;
                        break;

                        case '8':
                        numpower = 8;
                        break;

                        case '9':
                        numpower = 9;
                        break;

                        case '0':
                        numpower = 0;
                        break;

                        default: // impossible to reach here
                        numpower = 0;
                        break;
                    }
                    powerresult = findpower(strlen(replych)-counter-1);
                    reply += powerresult*numpower;
                }
                if (reply != 1 && reply != 2)
                {
                    cout << "Error! Invalid Input!" << endl;
                    cin.clear();
                    repeat = true;
                }
            }

        }while (repeat);
    }
    else if (victim != 0)
    {
        srand((unsigned)time(0));
        reply = (rand()%2)+1; // create from 1 to 2
    }

    if (reply == 2)
    {
        for (int draw = 0; draw < 4; draw++)
        {
            strcpy (p[victim].playcard[p[victim].num_card].name, card[distrindex].name);
            strcpy (p[victim].playcard[p[victim].num_card].colour, card[distrindex].colour);
            distrindex++;
            p[victim].num_card += 1;
        }
        if (victim == 0)
        {
            cout << "[DRAW FOUR ON YOU!!]" << endl;
            cout << "You get:" << endl;
            cout << "[" << p[victim].playcard[p[victim].num_card - 4].colour << "] [" << p[victim].playcard[p[victim].num_card - 4].name << "]" << endl;
            cout << "[" << p[victim].playcard[p[victim].num_card - 3].colour << "] [" << p[victim].playcard[p[victim].num_card - 3].name << "]" << endl;
            cout << "[" << p[victim].playcard[p[victim].num_card - 2].colour << "] [" << p[victim].playcard[p[victim].num_card - 2].name << "]" << endl;
            cout << "[" << p[victim].playcard[p[victim].num_card - 1].colour << "] [" << p[victim].playcard[p[victim].num_card - 1].name << "]" << endl;
        }
        else
        {
            cout << "Player " << victim+1 << " draws 4 cards" << endl;
        }
    }
    else if (reply == 1)
    {
        challengesuccess = false;
        cout << endl;
        cout << "CHALLENGE!!!" << endl;
        if (clockwise == true)
        {

            if (victim == 0)
                challenged = 3;
            else if (victim == 1)
                challenged = 0;
            else if (victim == 2)
                challenged = 1;
            else if (victim == 3)
                challenged = 2;
        }

        if (clockwise == false)
        {
            if (victim == 0)
                challenged = 1;
            else if (victim == 1)
                challenged = 2;
            else if (victim == 2)
                challenged = 3;
            else if (victim == 3)
                challenged = 0;
        }


            for (int i = 0; i < p[challenged].num_card; i++)
            {
                if (strcmpi(p[challenged].playcard[i].colour, previouscard.colour) == 0 ||
                    strcmpi(p[challenged].playcard[i].name, previouscard.name) == 0)
                {
                    challengesuccess = true;
                    break;
                }
                else
                    challengesuccess = false;
            }


        if (challengesuccess)
        {
            cout << "Challenge success!" << endl;
            if (clockwise)
            {
                token--;
            }
            else if (clockwise == false)
            {
                token++;
            }
            beingdraw = challenged;
            drawq = 4;
        }
        else if (challengesuccess == false)
        {
            cout << "Challenge failed!" << endl;
            beingdraw = victim;
            drawq = 6;
        }
        cout << endl;

        for (int draw = 0; draw < drawq; draw++)
        {
            strcpy (p[beingdraw].playcard[p[beingdraw].num_card].name, card[distrindex].name);
            strcpy (p[beingdraw].playcard[p[beingdraw].num_card].colour, card[distrindex].colour);
            distrindex++;
            p[beingdraw].num_card += 1;
        }
        if (beingdraw == 0)
        {
            if (drawq == 6)
            {
                cout << "[DRAW SIX (6) ON YOU!!]" << endl;
                cout << "You get:" << endl;
                cout << "[" << p[beingdraw].playcard[p[beingdraw].num_card - 6].colour << "] [" << p[beingdraw].playcard[p[beingdraw].num_card - 6].name << "]" << endl;
                cout << "[" << p[beingdraw].playcard[p[beingdraw].num_card - 5].colour << "] [" << p[beingdraw].playcard[p[beingdraw].num_card - 5].name << "]" << endl;
                cout << "[" << p[beingdraw].playcard[p[beingdraw].num_card - 4].colour << "] [" << p[beingdraw].playcard[p[beingdraw].num_card - 4].name << "]" << endl;
                cout << "[" << p[beingdraw].playcard[p[beingdraw].num_card - 3].colour << "] [" << p[beingdraw].playcard[p[beingdraw].num_card - 3].name << "]" << endl;
                cout << "[" << p[beingdraw].playcard[p[beingdraw].num_card - 2].colour << "] [" << p[beingdraw].playcard[p[beingdraw].num_card - 2].name << "]" << endl;
                cout << "[" << p[beingdraw].playcard[p[beingdraw].num_card - 1].colour << "] [" << p[beingdraw].playcard[p[beingdraw].num_card - 1].name << "]" << endl;
            }
            else if (drawq == 4)
            {
                cout << "[DRAW FOUR (4) ON YOU!!]" << endl;
                cout << "You get:" << endl;
                cout << "[" << p[beingdraw].playcard[p[beingdraw].num_card - 4].colour << "] [" << p[beingdraw].playcard[p[beingdraw].num_card - 4].name << "]" << endl;
                cout << "[" << p[beingdraw].playcard[p[beingdraw].num_card - 3].colour << "] [" << p[beingdraw].playcard[p[beingdraw].num_card - 3].name << "]" << endl;
                cout << "[" << p[beingdraw].playcard[p[beingdraw].num_card - 2].colour << "] [" << p[beingdraw].playcard[p[beingdraw].num_card - 2].name << "]" << endl;
                cout << "[" << p[beingdraw].playcard[p[beingdraw].num_card - 1].colour << "] [" << p[beingdraw].playcard[p[beingdraw].num_card - 1].name << "]" << endl;
            }
        }
        else
        {
            cout << "Player " << beingdraw+1 << " draws 6 cards" << endl;
        }
    }
}

void chooseColour (char cardontablecolour[], char cardontablename[], int num, player p)
{
    int delaytime;
    int randnum;
    char replych[100];
    bool repeat;
    int numpower, powerresult;
    int reply;

    int red = 0, yellow = 0, blue = 0, green = 0;

    if (num != 0)
    {
        for (int checkcolour = 0; checkcolour < p.num_card; checkcolour++)
        {
            if (strcmpi(p.playcard[checkcolour].colour,"Red") == 0)
                red++;
            else if (strcmpi(p.playcard[checkcolour].colour,"Yellow") == 0)
                yellow++;
            else if (strcmpi(p.playcard[checkcolour].colour,"Blue") == 0)
                blue++;
            else if (strcmpi(p.playcard[checkcolour].colour,"Green") == 0)
                green++;
        }
        if (red >= yellow && red >= blue && red >= green)
            randnum = 1;
        else if (yellow >= red && yellow >= blue && yellow >= green)
            randnum = 2;
        else if (blue >= red && blue >= yellow && blue >= green)
            randnum = 3;
        else if (green >= red && green >=yellow && green >= blue)
            randnum = 4;
        else
        {
            delaytime = 1;
            time_t start_time, cur_time;

            time(&start_time);
            do
            {
                time(&cur_time);
            }
            while((cur_time - start_time) < delaytime);

            srand((unsigned)time(0));
            randnum = (rand()%4)+1; // create from 0 to 4
        }

    }
    else if (num == 0)
    {
        cout << "\nPlease choose your colour:" << endl;
        cout << "1\tRed" << endl
            << "2\tYellow" << endl
            << "3\tBlue" << endl
            << "4\tGreen" << endl;
        do
        {
            repeat = false;
            reply = 0;
            cout << "Choice: ";
            cin.getline(replych, 100);
            if (strlen(replych)== 0)
            {
                cout << "Error! Enter something please" << endl;
                cin.clear();
                repeat = true;
            }


            for (int checkint = 0; checkint < strlen(replych); checkint++)
            {
                if (replych[checkint] != '1' &&
                    replych[checkint] != '2' &&
                    replych[checkint] != '3' &&
                    replych[checkint] != '4' &&
                    replych[checkint] != '5' &&
                    replych[checkint] != '6' &&
                    replych[checkint] != '7' &&
                    replych[checkint] != '8' &&
                    replych[checkint] != '9' &&
                    replych[checkint] != '0')
                {
                    cout << "Error! Enter only numbers!" << endl;
                    cin.clear();
                    repeat = true;
                    break;
                }
                else
                    repeat = false;
            }

            if (repeat == false)
            {
                for (int counter = 0; counter < strlen(replych); counter++)
                {
                    switch (replych[counter])
                    {
                        case '1':
                        numpower = 1;
                        break;

                        case '2':
                        numpower = 2;
                        break;

                        case '3':
                        numpower = 3;
                        break;

                        case '4':
                        numpower = 4;
                        break;

                        case '5':
                        numpower = 5;
                        break;

                        case '6':
                        numpower = 6;
                        break;

                        case '7':
                        numpower = 7;
                        break;

                        case '8':
                        numpower = 8;
                        break;

                        case '9':
                        numpower = 9;
                        break;

                        case '0':
                        numpower = 0;
                        break;

                        default: // impossible to reach here
                        numpower = 0;
                        break;
                    }
                    powerresult = findpower(strlen(replych)-counter-1);
                    reply += powerresult*numpower;
                }
                randnum = reply;

                if (randnum < 1 || randnum > 4)
                {
                    cout << "[Error! Invalid input]" << endl;
                    cin.clear();
                    repeat = true;
                }
            }
        }while (repeat);
    }
    switch (randnum)
    {
        case 1:
            strcpy (cardontablecolour, "Red");
            break;

        case 2:
            strcpy (cardontablecolour, "Yellow");
            break;

        case 3:
            strcpy (cardontablecolour, "Blue");
            break;

        case 4:
            strcpy (cardontablecolour, "Green");
            break;
    }
    strcpy(cardontablename, " "); // to ensure no similar card name can be played

    cout << "The user choose: " << cardontablecolour << endl;
}

void randomsort (unocard card[])
{
    unocard temp, temp2;
    int counter = 0;
    int delaytime = 1;
    int evenrand;
    for (int sorttime = 0; sorttime < 10; sorttime++)
    {
        cout << "...";
        time_t start_time, cur_time;
        time(&start_time);
        do
        {
            time(&cur_time);
        }
        while((cur_time - start_time) < delaytime);
        srand((unsigned)time(0));
        evenrand = (rand()%7)+3;

        for (int repeat = 0; repeat < 500; repeat++)
        {
            for (int sort = 0; sort < 108; sort++)
            {
                strcpy(temp.name, card[sort].name);
                strcpy(temp.colour, card[sort].colour);
                strcpy(card[sort].name, card[evenrand].name);
                strcpy(card[sort].colour, card[evenrand].colour);
                strcpy(card[evenrand].name, temp.name);
                strcpy(card[evenrand].colour, temp.colour);

                if (evenrand >= 108-6)
                    evenrand -= 3;
                else if (evenrand >= 0)
                    evenrand += 5;
                else
                    evenrand += 20;
            }
        }
    }

    evenrand = 0;

    for (int sorttime = 0; sorttime < 5; sorttime++)
    {
        cout << "....";
        time_t start_time, cur_time;
        time(&start_time);
        do
        {
            time(&cur_time);
        }
        while((cur_time - start_time) < delaytime);
        srand((unsigned)time(0));
        evenrand = (rand()%7)+15;

        for (int repeat = 0; repeat < 700; repeat++)
        {
            for (int sort = 0; sort < 108; sort++)
            {

                strcpy(temp.name, card[sort].name);
                strcpy(temp.colour, card[sort].colour);
                strcpy(card[sort].name, card[evenrand].name);
                strcpy(card[sort].colour, card[evenrand].colour);
                strcpy(card[evenrand].name, temp.name);
                strcpy(card[evenrand].colour, temp.colour);

                if (evenrand >= 108-21)
                    evenrand -= 12;
                else if (evenrand >= 0)
                    evenrand += 20;
                else
                    evenrand += 50;
            }
        }
    }

    evenrand = 0;

    for (int sorttime = 0; sorttime < 5; sorttime++)
    {
        cout << "...";
        time_t start_time, cur_time;
        time(&start_time);
        do
        {
            time(&cur_time);
        }
        while((cur_time - start_time) < delaytime);
        srand((unsigned)time(0));
        evenrand = (rand()%7)+10;


        for (int repeat = 0; repeat < 1000; repeat++)
        {
            for (int sort = 0; sort < 108; sort++)
            {

                strcpy(temp.name, card[sort].name);
                strcpy(temp.colour, card[sort].colour);
                strcpy(card[sort].name, card[evenrand].name);
                strcpy(card[sort].colour, card[evenrand].colour);
                strcpy(card[evenrand].name, temp.name);
                strcpy(card[evenrand].colour, temp.colour);

                if (evenrand >= 108-13)
                    evenrand -= 8;
                else if (evenrand >= 0)
                    evenrand += 12;
                else
                    evenrand += 30;
            }
        }
    }

    /*for (int sorttime = 0; sorttime < 2; sorttime++)
    {
        cout << "......";
        time_t start_time, cur_time;
        time(&start_time);
        do
        {
            time(&cur_time);
        }
        while((cur_time - start_time) < delaytime);
        srand((unsigned)time(0));
        evenrand = (rand()%17)+2;

        for (int repeat = 0; repeat < 1079; repeat++)
        {
            for (int sort = 0; sort < 108; sort++)
            {
                strcpy(temp.name, card[sort].name);
                strcpy(temp.colour, card[sort].colour);
                strcpy(card[sort].name, card[evenrand].name);
                strcpy(card[sort].colour, card[evenrand].colour);
                strcpy(card[evenrand].name, temp.name);
                strcpy(card[evenrand].colour, temp.colour);

                if (evenrand >= 108-6)
                    evenrand -= 3;
                else if (evenrand >= 0)
                    evenrand += 5;
                else
                    evenrand += 12;
            }
        }
    }


    evenrand = 0;

    for (int sorttime = 0; sorttime < 4; sorttime++)
    {
        cout << "......";
        time_t start_time, cur_time;
        time(&start_time);
        do
        {
            time(&cur_time);
        }
        while((cur_time - start_time) < delaytime);
        srand((unsigned)time(0));
        evenrand = (rand()%12)+13;

        for (int repeat = 0; repeat < 539; repeat++)
        {
            for (int sort = 0; sort < 108; sort++)
            {

                strcpy(temp.name, card[sort].name);
                strcpy(temp.colour, card[sort].colour);
                strcpy(card[sort].name, card[evenrand].name);
                strcpy(card[sort].colour, card[evenrand].colour);
                strcpy(card[evenrand].name, temp.name);
                strcpy(card[evenrand].colour, temp.colour);

                if (evenrand >= 108-11)
                    evenrand -= 12;
                else if (evenrand >= 0)
                    evenrand += 9;
                else
                    evenrand += 23;
            }
        }
    }

    evenrand = 0;

    for (int sorttime = 0; sorttime < 5; sorttime++)
    {
        cout << "......";
        time_t start_time, cur_time;
        time(&start_time);
        do
        {
            time(&cur_time);
        }
        while((cur_time - start_time) < delaytime);
        srand((unsigned)time(0));
        evenrand = (rand()%)+16;


        for (int repeat = 0; repeat < 327; repeat++)
        {
            for (int sort = 0; sort < 108; sort++)
            {

                strcpy(temp.name, card[sort].name);
                strcpy(temp.colour, card[sort].colour);
                strcpy(card[sort].name, card[evenrand].name);
                strcpy(card[sort].colour, card[evenrand].colour);
                strcpy(card[evenrand].name, temp.name);
                strcpy(card[evenrand].colour, temp.colour);

                if (evenrand >= 108-13)
                    evenrand -= 8;
                else if (evenrand >= 0)
                    evenrand += 12;
                else
                    evenrand += 17;
            }
        }
    }*/
}
void coloursort (player p[])
{
    char tempcolour[100],tempname[100];
    int colourindex = 0;
    int numindex = 0;
    int tempindex = 0;
    for (int check1 = 0; check1 < p[0].num_card; check1++)
    {
        if (strcmpi(p[0].playcard[check1].colour,"red") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check1].colour);
            strcpy(tempname,p[0].playcard[check1].name);
            strcpy(p[0].playcard[check1].colour, p[0].playcard[colourindex].colour);
            strcpy(p[0].playcard[check1].name, p[0].playcard[colourindex].name);
            strcpy(p[0].playcard[colourindex].colour, tempcolour);
            strcpy(p[0].playcard[colourindex].name, tempname);
            colourindex++;
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////
    for (int check2 = 0; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "0") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    for (int check2 = 0; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "1") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    for (int check2 = 0; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "2") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    for (int check2 = 0; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "3") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    for (int check2 = 0; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "4") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    for (int check2 = 0; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "5") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    for (int check2 = 0; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "6") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    for (int check2 = 0; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "7") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    for (int check2 = 0; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "8") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    for (int check2 = 0; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "9") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    for (int check2 = 0; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "skip") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    for (int check2 = 0; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "draw two") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    for (int check2 = 0; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "reverse") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    tempindex = colourindex;

    ///////////////////////////////////////////////////////////////////////////////////////////


    for (int check1 = 0; check1 < p[0].num_card; check1++)
    {
        if (strcmpi(p[0].playcard[check1].colour,"yellow") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check1].colour);
            strcpy(tempname,p[0].playcard[check1].name);
            strcpy(p[0].playcard[check1].colour, p[0].playcard[colourindex].colour);
            strcpy(p[0].playcard[check1].name, p[0].playcard[colourindex].name);
            strcpy(p[0].playcard[colourindex].colour, tempcolour);
            strcpy(p[0].playcard[colourindex].name, tempname);
            colourindex++;
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////

    for (int check2 = tempindex; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "0") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    for (int check2 = tempindex; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "1") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    for (int check2 = tempindex; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "2") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    for (int check2 = tempindex; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "3") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    for (int check2 = tempindex; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "4") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    for (int check2 = tempindex; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "5") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    for (int check2 = tempindex; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "6") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    for (int check2 = tempindex; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "7") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    for (int check2 = tempindex; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "8") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    for (int check2 = tempindex; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "9") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    for (int check2 = tempindex; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "skip") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    for (int check2 = tempindex; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "draw two") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    for (int check2 = tempindex; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "reverse") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    tempindex = colourindex;
    ///////////////////////////////////////////////////////////////////////////////////////////


    for (int check1 = 0; check1 < p[0].num_card; check1++)
    {
        if (strcmpi(p[0].playcard[check1].colour,"blue") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check1].colour);
            strcpy(tempname,p[0].playcard[check1].name);
            strcpy(p[0].playcard[check1].colour, p[0].playcard[colourindex].colour);
            strcpy(p[0].playcard[check1].name, p[0].playcard[colourindex].name);
            strcpy(p[0].playcard[colourindex].colour, tempcolour);
            strcpy(p[0].playcard[colourindex].name, tempname);
            colourindex++;
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////

    for (int check2 = tempindex; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "0") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    for (int check2 = tempindex; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "1") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    for (int check2 = tempindex; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "2") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    for (int check2 = tempindex; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "3") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    for (int check2 = tempindex; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "4") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    for (int check2 = tempindex; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "5") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    for (int check2 = tempindex; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "6") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    for (int check2 = tempindex; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "7") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    for (int check2 = tempindex; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "8") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    for (int check2 = tempindex; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "9") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    for (int check2 = tempindex; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "skip") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    for (int check2 = tempindex; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "draw two") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    for (int check2 = tempindex; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "reverse") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    tempindex = colourindex;
    ///////////////////////////////////////////////////////////////////////////////////////////


    for (int check1 = 0; check1 < p[0].num_card; check1++)
    {
        if (strcmpi(p[0].playcard[check1].colour,"green") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check1].colour);
            strcpy(tempname,p[0].playcard[check1].name);
            strcpy(p[0].playcard[check1].colour, p[0].playcard[colourindex].colour);
            strcpy(p[0].playcard[check1].name, p[0].playcard[colourindex].name);
            strcpy(p[0].playcard[colourindex].colour, tempcolour);
            strcpy(p[0].playcard[colourindex].name, tempname);
            colourindex++;
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////

    for (int check2 = tempindex; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "0") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    for (int check2 = tempindex; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "1") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    for (int check2 = tempindex; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "2") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    for (int check2 = tempindex; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "3") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    for (int check2 = tempindex; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "4") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    for (int check2 = tempindex; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "5") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    for (int check2 = tempindex; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "6") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    for (int check2 = tempindex; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "7") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    for (int check2 = tempindex; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "8") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    for (int check2 = tempindex; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "9") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    for (int check2 = tempindex; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "skip") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    for (int check2 = tempindex; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "draw two") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    for (int check2 = tempindex; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "reverse") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    tempindex = colourindex;
    ///////////////////////////////////////////////////////////////////////////////////////////


    for (int check1 = 0; check1 < p[0].num_card; check1++)
    {
        if (strcmpi(p[0].playcard[check1].colour,"black") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check1].colour);
            strcpy(tempname,p[0].playcard[check1].name);
            strcpy(p[0].playcard[check1].colour, p[0].playcard[colourindex].colour);
            strcpy(p[0].playcard[check1].name, p[0].playcard[colourindex].name);
            strcpy(p[0].playcard[colourindex].colour, tempcolour);
            strcpy(p[0].playcard[colourindex].name, tempname);
            colourindex++;
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////

    for (int check2 = tempindex; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "wild") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }
    for (int check2 = tempindex; check2 < colourindex; check2++)
    {
        if (strcmpi(p[0].playcard[check2].name, "draw four") == 0)
        {
            strcpy(tempcolour,p[0].playcard[check2].colour);
            strcpy(tempname,p[0].playcard[check2].name);
            strcpy(p[0].playcard[check2].colour, p[0].playcard[numindex].colour);
            strcpy(p[0].playcard[check2].name, p[0].playcard[numindex].name);
            strcpy(p[0].playcard[numindex].colour, tempcolour);
            strcpy(p[0].playcard[numindex].name, tempname);
            numindex++;
        }
    }

    tempindex = colourindex;
    ///////////////////////////////////////////////////////////////////////////////////////////

}

int findpower (int power_n)
{
    int result = 1;
	for (int i = power_n; i > 0; i--)
        result *= 10;
    return result;
}
